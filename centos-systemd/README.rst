Packer template to build a centOS systemd docker image
======================================================

This template creates a Docker_ image based on `CentOS 7 <https://hub.docker.com/r/_/centos/>`_ with systemd enabled.

This image allows to test Ansible_ roles that use the *service* or *systemd* module
in a docker container (using Molecule_).
The goal is not to deploy an application in a docker container running systemd.

This is for testing purpose only.


Usage
-----

To be able to push the image to Docker Hub, you first have to login::

    $ docker login

You can then run::

    $ packer build centos-systemd.json

This will create and push the image `europeanspallationsource/centos-systemd:7` based on centos:7 image.

Note that you can pass a specific tag as variable.
To create an image based on centos:7.3.1611 image::

    $ packer build -var "centos_tag=7.3.1611" centos-systemd.json

The resulting image will have the same tag as the base centos one.


.. _Docker: https://www.docker.com
.. _Ansible: http://docs.ansible.com/ansible/
.. _Molecule: https://molecule.readthedocs.io
