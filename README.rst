ESS ICS Packer templates
========================

This repository includes Packer_ templates used by ICS.

.. _Packer: https://www.packer.io
